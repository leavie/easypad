package easyNotePad;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author leavie
 */
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class EasyNotePad {

    private Frame frame;
    private MenuBar mb;
    private Menu fileMenu, help, edit;
    private MenuItem openItem, saveItem, closeItem, saveAsItem, newWindowItem;

    private TextArea textArea;
    private FileDialog saveDial, openDial;

    private File file;
    private Document document;

    private Stack<Command> commandHistory;

    int caretPosition;

    public EasyNotePad() {
        commandHistory = new Stack<>();
        caretPosition = 0;

        initUI();
        setHandler();
        setComplicatedHandler();
    }

    private void addHelpMenu() {
        help = new Menu("help");
        Menu about = new Menu("about");
        Menu sample = new Menu("samelp");

        about.add(new MenuItem("Creator"));
        sample.add(new MenuItem("example"));
        help.add(about);
        help.addSeparator();
        help.add(sample);

    }

    private void addEditMenu() { // cant not cooperate with system shorcut cut paste
        edit = new Menu("edit");
        final MenuItem undo = new MenuItem("undo", new MenuShortcut(
                KeyEvent.VK_Z));
        edit.add(undo);
        final MenuItem cut = new MenuItem("cut", new MenuShortcut(KeyEvent.VK_X));
        edit.add(cut);
        final MenuItem paste = new MenuItem("paste", new MenuShortcut(
                KeyEvent.VK_V));
        edit.add(paste);

        undo.addActionListener((l) -> {
            executeCommand(new UndoCommand(EasyNotePad.this,
                    textArea));
        });
        cut.addActionListener((l) -> {
            executeCommand(
                    new CutCommand(EasyNotePad.this, textArea));
        });
        paste.addActionListener((l) -> {
            executeCommand(new PasteCommand(EasyNotePad.this,
                    textArea));
        });
    }

    private void setComplicatedHandler() {
        textArea.addTextListener((l) -> {
            caretPosition = textArea.getCaretPosition();
        });
        textArea.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (caretPosition != textArea.getCaretPosition()) {
                    System.out.println(
                            "old caretPosition:" + caretPosition + ",current textArea.getCaretPosition():" + textArea.getCaretPosition());
                    executeCommand(new CaretMoveCommand(EasyNotePad.this,
                            textArea));
                    caretPosition = textArea.getCaretPosition();
                }

            }

        });
        textArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
//                System.out.println("text area content:" + textArea.getText());
                System.out.println("pressed key:" + e.getKeyCode());
                if (e.isMetaDown()) { // with meta modifier
                    System.out.println("with meta modifier");
                    if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_V) {
                        executeCommand(new PasteCommand(EasyNotePad.this,
                                textArea));
                    } else if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_X) {
                        executeCommand(
                                new CutCommand(EasyNotePad.this, textArea));
                    } else if (e.isMetaDown() && e.getKeyCode() == KeyEvent.VK_Z) {
                        executeCommand(new UndoCommand(EasyNotePad.this,
                                textArea));
                    }

                } else { // without meta modifier
                    System.out.println("without meta modifier");
                    if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE) {
                        executeCommand(new DeleteCommand(EasyNotePad.this,
                                textArea));
                    } else {
                        executeCommand(new Typing(EasyNotePad.this, textArea));
                    }
                }
            }

        });
    }

    private void initUI() throws HeadlessException {
        frame = new Frame("Editor");
        frame.setBounds(100, 100, 600, 600);

        mb = new MenuBar();

        frame.setMenuBar(mb);

        fileMenu = new Menu("file");

        mb.add(fileMenu);
        addHelpMenu();
        mb.add(help);
//        addEditMenu();
//        mb.add(edit);

        openItem = new MenuItem("open");
        openItem.setShortcut(new MenuShortcut(KeyEvent.VK_O, false));
        openItem.setActionCommand("...");

        saveItem = new MenuItem("save", new MenuShortcut(KeyEvent.VK_S, false));

        saveAsItem = new MenuItem("save as", new MenuShortcut(KeyEvent.VK_S,
                true));
        closeItem = new MenuItem("close");

        newWindowItem = new MenuItem("new window", new MenuShortcut(
                KeyEvent.VK_N, false));

        fileMenu.add(openItem);
        fileMenu.add(saveItem);
        fileMenu.add(saveAsItem);
        fileMenu.add(closeItem);
        fileMenu.add(newWindowItem);
        textArea = new TextArea();

        frame.add(textArea);
        frame.setVisible(true);

        saveDial = new FileDialog(frame, "save", FileDialog.SAVE);
        openDial = new FileDialog(frame, "open", FileDialog.LOAD);
    }

    private void setHandler() {
        saveItem.addActionListener((l) -> {
            save(false);
        });
        saveAsItem.addActionListener((l) -> {
            save(true);
        });
        openItem.addActionListener((l) -> {
            System.out.println("action command name:" + l.getActionCommand());
            open();
        });
        closeItem.addActionListener((l) -> {
            System.exit(0);
        });

        newWindowItem.addActionListener((l) -> {
            newWindow();
        });

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    public void save(boolean as) {
        if (file == null || as) {
            saveDial.setVisible(true);
            String dirpath = saveDial.getDirectory();
            String filename = saveDial.getFile();
            System.out.println("dir:" + dirpath + ",file:" + filename);

            if (dirpath == null && filename == null) {
                return;
            }
            frame.setTitle(filename);
            file = new File(dirpath, filename);
            document = new Document(file);
        }
        document.setText(textArea.getText());
        document.save();
    }

    public void open() {
        openDial.setVisible(true);
        String dirpath = openDial.getDirectory();
        String filename = openDial.getFile();

        System.out.println("dir:" + dirpath + ",file:" + filename);

        if (dirpath == null && filename == null) {
            return;
        }

        frame.setTitle(filename);
        file = new File(dirpath, filename);
        document = new Document(file);
        textArea.setText(document.getText());

        commandHistory.clear();
    }

    public void newWindow() {
//        Frame f = new Frame("Editor");
//        f.setBounds(100, 100, 600, 600);
//        f.setVisible(true);
        new EasyNotePad();
    }

    public void executeCommand(Command command) {
        if (command.execute()) {
            if (commandHistory.isEmpty() || command.repeatable || !command.equals(
                    commandHistory.peek())) {
                commandHistory.push(command);
            }
        }
    }

    public void undoCommandHistory() {
        if (!commandHistory.isEmpty()) {
            Command command = commandHistory.pop();
            command.undo();
        }
    }

    public static void main(String[] args) {
        new EasyNotePad();
    }
}

abstract class Command {

    String typeName;
    String backup = "";
    int caretPosition = 0;
    boolean repeatable = true;
    EasyNotePad app;
    java.awt.TextArea textArea;

    public Command(EasyNotePad app, TextArea textArea) {
        this.app = app;
        this.textArea = textArea;
    }

    @Override
    public boolean equals(Object obj) {
        return typeName == ((Command) obj).typeName;
    }

    boolean execute() {
        System.out.println(typeName + " command" + " execute");
        return executeSpecific();
    }

    abstract boolean executeSpecific(); // boolean represents notifying command manager whether adding this command to command history or not

    void undo() {
        System.out.println(typeName + " command" + " undo");
        textArea.setText(backup);
        textArea.setCaretPosition(caretPosition);
    }


}

class CutCommand extends Command {

    public CutCommand(EasyNotePad app, TextArea textArea) {
        super(app, textArea);
        typeName = "cut";
    }

    @Override
    public boolean executeSpecific() {
        backup = textArea.getText();
        caretPosition = textArea.getCaretPosition();
        return true;
    }

}

class PasteCommand extends Command {

    public PasteCommand(EasyNotePad app, TextArea textArea) {
        super(app, textArea);
        typeName = "paste";
    }

    @Override
    public boolean executeSpecific() {
        backup = textArea.getText();
        caretPosition = textArea.getCaretPosition();
        return true;
    }

}

class UndoCommand extends Command {

    public UndoCommand(EasyNotePad app, TextArea textArea) {
        super(app, textArea);
        typeName = "undo";
    }

    @Override
    public boolean executeSpecific() {
        app.undoCommandHistory();
        return false;
    }

}

class DeleteCommand extends Command {

    public DeleteCommand(EasyNotePad app, TextArea textArea) {
        super(app, textArea);
        repeatable = false;
        typeName = "delete";
    }

    @Override
    public boolean executeSpecific() {
        backup = textArea.getText();
        caretPosition = textArea.getCaretPosition();
        return true;
    }
}

class CaretMoveCommand extends Command {

    public CaretMoveCommand(EasyNotePad app, TextArea textArea) {
        super(app, textArea);
        typeName = "caretMove";
    }

    @Override
    boolean executeSpecific() {
        backup = this.textArea.getText();
        caretPosition = app.caretPosition;
        return true;
    }

}

class Typing extends Command {

    public Typing(EasyNotePad app, TextArea textArea) {
        super(app, textArea);
        repeatable = false;
        typeName = "typing";
    }

    @Override
    boolean executeSpecific() {
        backup = this.textArea.getText();
        caretPosition = textArea.getCaretPosition();

        return true;
    }

}
