/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyNotePad;

import java.io.*;
import java.util.Scanner;
import java.util.logging.*;

/**
 *
 * @author leavie
 */
public class Document {

    private String text;
    private File file;

    public Document() {
    }

    public Document(File file) {
        this.file = file;

        StringBuilder sb = new StringBuilder();
        try (Scanner reader = new Scanner(new BufferedInputStream(
                new FileInputStream(file)));) {
            while (reader.hasNext()) {
                sb.append(reader.nextLine()).append("\n");

            }
            this.text = (sb.toString());

        } catch (FileNotFoundException ex) {
            Logger.getLogger(EasyNotePad.class.getName()).log(Level.SEVERE, null,
                    ex);
        }
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }


    public boolean save() {
        if (file == null) return false;
        return save(file);
    }

    public boolean save(File file) {
        try (OutputStream writer = new BufferedOutputStream(
                new FileOutputStream(
                        file));) { // try-resources (auto close resources)

            writer.write(getText().getBytes());
            return true;
        } catch (IOException ex) {
            Logger.getLogger(EasyNotePad.class.getName()).log(Level.SEVERE, null,
                    ex);
            return false;
        }
    }

}
